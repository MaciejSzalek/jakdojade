import { TestBed } from '@angular/core/testing';

import { BikestationService } from './bikestation.service';

describe('BikestationService', () => {
  let service: BikestationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BikestationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
