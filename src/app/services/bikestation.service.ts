import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class BikestationService {
  stationsData: any;

  constructor(private _http: HttpClient) {}

  getStations() {
    return this._http.get(environment.bikestationsURL);
  }

  getSingleStation(stationId: string) {
    return this._http.get(environment.bikestationsURL).pipe(
      map((data: any) => {
        return data.features.find(el => {
          return el.id === stationId;
        });
      })
    );
  }

  calculateDistance(lat1:number, lon1:number, lat2:number, lon2:number) {
    if (lat1 == lat2 && lon1 == lon2) {
      return 0;
    } else {
      let radlat1 = (Math.PI * lat1) / 180;
      let radlat2 = (Math.PI * lat2) / 180;
      let theta = lon1 - lon2;
      let radtheta = (Math.PI * theta) / 180;
      let dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;
      dist = dist * 1.609344;
      dist = Math.floor(dist * 1000);
      return dist;
    }
  }
}
