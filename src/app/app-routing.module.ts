import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StationsListComponent } from "./components/stations-list/stations-list.component";
import { StationDetailComponent } from "./components/station-detail/station-detail.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "stations"
  },
  {
    path: "stations",
    children: [
      {
        path: "",
        component: StationsListComponent
      },
      {
        path: ":id",
        component: StationDetailComponent
      }
    ]
  },
  {
    path: "404",
    component: NotFoundComponent
  },
  {
    path: "**",
    redirectTo: "/404"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
