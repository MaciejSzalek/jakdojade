import { Component, OnInit } from "@angular/core";
import { BikestationService } from "../../services/bikestation.service";
import { ReplaySubject } from "rxjs";

@Component({
  selector: "app-stations-list",
  templateUrl: "./stations-list.component.html",
  styleUrls: ["./stations-list.component.scss"]
})
export class StationsListComponent implements OnInit {
  bikestations: any = [];
  bikestationsChange: ReplaySubject<boolean> = new ReplaySubject(0);

  constructor(private _bikestationService: BikestationService) {
    this.getCoordinates = this.getCoordinates.bind(this);
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.getCoordinates);
    }
  }

  ngOnInit(): void {
    this._bikestationService.getStations().subscribe((data: any) => {
      this.bikestations = data.features;
      this.bikestationsChange.next(true);
    });
  }

  getCoordinates(position: any): void {
    const lat = position.coords.latitude;
    const lng = position.coords.longitude;
    if (this.bikestations.length !== 0) {
      this.sortStations(lat, lng);
    } else {
      this.bikestationsChange.subscribe(data => {
        this.sortStations(lat, lng);
      });
    }
  }

  sortStations(myLat: number, myLng: number): void {
    this.bikestations = this.bikestations.map((el:any) => {
      el.distance = this._bikestationService.calculateDistance(
        el.geometry.coordinates[1],
        el.geometry.coordinates[0],
        myLat,
        myLng
      );
      return el;
    });
    this.bikestations.sort((firstEl: any, secondEl: any) => {
      return firstEl.distance - secondEl.distance;
    });
  }
}
