import { Component, AfterViewInit, ViewChild, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BikestationService } from "../../services/bikestation.service";
import { environment } from "../../../environments/environment";
import { ReplaySubject } from "rxjs";
import { Map, Marker, Popup, GeolocateControl } from "mapbox-gl";

@Component({
  selector: "app-station-detail",
  templateUrl: "./station-detail.component.html",
  styleUrls: ["./station-detail.component.scss"]
})
export class StationDetailComponent implements AfterViewInit {
  @ViewChild("mapElement", { static: false }) public mapElement: ElementRef;
  currentUrl: any;
  station: any;
  stationChange: ReplaySubject<boolean> = new ReplaySubject(0);
  map: mapboxgl.Map;
  style = "mapbox://styles/mapbox/light-v10";

  constructor(
    private _bikestationService: BikestationService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.currentUrl = this._route.snapshot.params;
    this._bikestationService
      .getSingleStation(this.currentUrl.id)
      .subscribe((data: any) => {
        if (data) {
          this.station = data;
          this.stationChange.next(true);
        } else {
          this._router.navigateByUrl("/404");
        }
      });

    this.getCoordinates = this.getCoordinates.bind(this);
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.getCoordinates);
    }
  }

  ngAfterViewInit(): void {
    this.map = new Map({
      accessToken: environment.mapbox.accessToken,
      container: this.mapElement.nativeElement,
      style: this.style,
      zoom: 15,
      center: [0, 0]
    });
    this.map.resize();
    this.map.addControl(
      new GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true
        },
        trackUserLocation: true
      })
    );

    this.stationChange.subscribe(data => {
      this.map.jumpTo({
        center: this.station.geometry.coordinates
      });
      let markerEl = document.createElement("div");
      markerEl.className = "marker";
      new Marker(markerEl)
        .setLngLat([
          this.station.geometry.coordinates[0],
          this.station.geometry.coordinates[1]
        ])
        .setPopup(
          new Popup({
            closeOnClick: false,
            closeButton: false,
            offset: [32, 27]
          }).setHTML("<h1>" + this.station.properties.bikes + "</h1>")
        )
        .addTo(this.map)
        .togglePopup();
    });
  }

  getCoordinates(position: any): void {
    const lat = position.coords.latitude;
    const lng = position.coords.longitude;
    if (this.station) {
      this.station.distance = this._bikestationService.calculateDistance(
        lat,
        lng,
        this.station.geometry.coordinates[1],
        this.station.geometry.coordinates[0]
      );
    } else {
      this.stationChange.subscribe(data => {
        this.station.distance = this._bikestationService.calculateDistance(
          lat,
          lng,
          this.station.geometry.coordinates[1],
          this.station.geometry.coordinates[0]
        );
      });
    }
  }
}
