import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-station-card",
  templateUrl: "./station-card.component.html",
  styleUrls: ["./station-card.component.scss"]
})
export class StationCardComponent {
  @Input() stationId: number;
  @Input() isListCard: boolean;

  constructor(private _router: Router) {}

  openStationDetails() {
    if (this.isListCard) {
      this._router.navigate(["/stations/", this.stationId]);
    }
  }
}
