import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { InlineSVGModule } from "ng-inline-svg";
import { MatCardModule } from "@angular/material/card";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { MatDividerModule } from "@angular/material/divider";
import { MatSelectModule } from "@angular/material/select";
import { MatDialogModule } from "@angular/material/dialog";
import { MatGridListModule } from "@angular/material/grid-list";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { StationsListComponent } from "./components/stations-list/stations-list.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { HeaderComponent } from "./components/header/header.component";
import { StationDetailComponent } from "./components/station-detail/station-detail.component";
import { BikestationService } from "./services/bikestation.service";
import { RequestCacheService } from "./services/request-cache.service";
import { MatToolbarModule } from "@angular/material/toolbar";
import { StationCardComponent } from "./components/station-card/station-card.component";
import { CacheInterceptor } from "./interceptors/cache.interceptor";

@NgModule({
  declarations: [
    AppComponent,
    StationsListComponent,
    NotFoundComponent,
    HeaderComponent,
    StationDetailComponent,
    StationCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    }),
    HttpClientModule,
    InlineSVGModule,
    // material modules
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
    MatDividerModule,
    MatToolbarModule,
    MatSelectModule,
    MatDialogModule,
    MatGridListModule
  ],
  providers: [
    BikestationService,
    RequestCacheService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CacheInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
