export const environment = {
  production: true,
  bikestationsURL:
    "https://www.poznan.pl/mim/plan/map_service.html?mtype=pub_transport&co=stacje_rowerowe",
  mapbox: {
    accessToken:
      "pk.eyJ1IjoibWFzemEiLCJhIjoiY2s2bWZ5NDA0MDBuczNubWEwcGw3Yms3NyJ9.hy_0mT84Uvdnet0LYDDoyA"
  }
};
